package com.example.preet.rolldice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {


    private Button rollButtonOne;
    private Button rollButtonTwo;
    private TextView firstResult;
    private TextView secondResult;
    int low = 1;
    int high = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstResult = findViewById(R.id.textView);
        secondResult = findViewById(R.id.textView4);
        rollButtonOne = findViewById(R.id.rollButtonOne);
        rollButtonTwo = findViewById(R.id.rollButtonTwo);


        View.OnClickListener rollOnceListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               oneButtonRoll();



            }
        };

        View.OnClickListener rollTwiceListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oneButtonRoll();
                Random r = new Random();
                int result = r.nextInt(high-low)+low;
                secondResult.setText(Integer.toString(result));
            }
        };

        rollButtonOne.setOnClickListener(rollOnceListener);
        rollButtonTwo.setOnClickListener(rollTwiceListener);

    }

    private void oneButtonRoll(){
        secondResult.setText("");
        Random r = new Random();
        int result = r.nextInt(high-low)+low;
        firstResult.setText(Integer.toString(result));

    }
}
